import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, EventEmitter,
  OnInit,
} from '@angular/core';
import { IPagination, ZbDialog } from '@zb/suite-core';
import { GroupService } from './group.service';
import { IGroup } from './models/IGroup';
import { CreateGroupDialogComponent } from './create-group/create-group-dialog.component';
import {forkJoin} from "rxjs";
import {FormGroup} from "@angular/forms";

@Component({
  selector: 'group-container',
  templateUrl: './group-container.component.html',
  styleUrls: ['./group-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GroupContainerComponent implements OnInit {
  groups: IGroup[];
  pagination: IPagination;
  teachers;

  constructor(
    private groupService: GroupService,
    private cdr: ChangeDetectorRef,
    private dialog: ZbDialog
  ) {}

  openDialog(): void {
    const yearsData = this.groupService.getYears();
    const periodsData = this.groupService.getPeriods();

    forkJoin([yearsData, periodsData])
      .subscribe(([years, periods]) => {
        const ref = this.dialog.open(CreateGroupDialogComponent, {
          closeOnEscKey: true,
          closeOnBackdropClick: true,
          dialogData: {
            years,
            periods,
          }
        });

        ref.componentInstance.dialogData.yearChange.subscribe(({form}) => {
          const {year, period} = (form as FormGroup).getRawValue();

          if (year && period) {
            this.groupService.getProgramTypes({year, period})
              .subscribe(programTypes => ref.componentInstance.dialogData.programTypes = programTypes);
          }
        });
        ref.componentInstance.dialogData.periodChange.subscribe(({form}) => {
          const {year, period} = (form as FormGroup).getRawValue();

          if (year && period) {
            this.groupService.getProgramTypes({year, period})
              .subscribe(programTypes => ref.componentInstance.dialogData.programTypes = programTypes);
          }
        });
        ref.componentInstance.dialogData.programTypeChange.subscribe(({value: programType, form}) => {
          const {year, period} = (form as FormGroup).getRawValue();

          if (year && period && programType) {
            this.groupService.getPrograms({year, period, programType})
              .subscribe(programs => ref.componentInstance.dialogData.programs = programs);
          }
        });
        ref.componentInstance.dialogData.programChange.subscribe(({value}) => {
          this.groupService.getCurriculums(value).subscribe(curriculums => ref.componentInstance.dialogData.curriculums = curriculums);
        });
        ref.componentInstance.dialogData.curriculumChange.subscribe(({value, form}) => {
          this.groupService.getJournals(value).subscribe(journals => form.setJournals(journals));
        });
        ref.componentInstance.dialogData.create.subscribe(({value}) => {
          this.groupService.createGroup(value).subscribe(_ => ref.close(_));
        });
      });
  }

  ngOnInit() {
    this.groupService
      .getGroups()
      .subscribe(
        ({
          content: groups,
          totalElements,
          pageable: { offset, pageSize },
        }) => {
          this.groups = groups;
          this.pagination = {
            total: totalElements,
            first: totalElements > 0 ? offset + 1 : 0,
            last: Math.min(offset + pageSize, totalElements),
          };
        }
      );
  }

  getSubjects(groupId: number) {
    this.groupService.getSubjects(groupId).subscribe((response) => {
      this.groups = this.groups.map((group) => ({ ...group, active: false }));
      const currentGroupIndex = this.groups.findIndex(
        (group) => group.id === groupId
      );
      const group = this.groups[currentGroupIndex];
      group.active = true;
      group.subjects = response;
      this.groups.splice(currentGroupIndex, 1, group);
      this.groups = [...this.groups];
      this.cdr.detectChanges();
    });
  }

  onUpdateSubjects({ subjects, groupId }) {
    this.groupService.updateSubjects(subjects, groupId).subscribe(() => {
      this.groups = this.groups.map((group) => ({ ...group, active: false }));
      this.cdr.detectChanges();
    });
  }
}
