export interface ITypeProgram {
  code: string;
  id: number;
  name: string;
  status: string;
}
