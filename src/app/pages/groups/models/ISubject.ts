export interface ISubject {
  assignedHours: number;
  capacity: number;
  curriculumItemId: number;
  enrolledQuantity: number;
  id: number;
  modality: string;
  pendingHours: number;
  status: string;
  subjectCode: string;
  subjectId: number;
  subjectName: string;
  subjectScheduleType: string;
  teacherId: number;
  teacherIdentification: string;
  teacherName: string;
  totalHours: number;
}
