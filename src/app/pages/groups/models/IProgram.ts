import {IFaculty} from './IFaculty';
import {ITypeProgram} from './ITypeProgram';

export interface IProgram {
  admissionRequired: boolean;
  code: string;
  creationDate: string;
  faculty: IFaculty;
  id: number;
  modality: string;
  name: string;
  payInscription: boolean;
  snies: string;
  status: string;
  title: string;
  typeProgram: ITypeProgram;
}
