import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { IGroup } from '../../models/IGroup';

@Component({
  selector: 'group',
  templateUrl: 'group.component.html',
  styleUrls: ['./group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GroupComponent {
  @Input() group: IGroup;
  @Input() teachers: any[];

  @Output() searchSubjects: EventEmitter<void> = new EventEmitter();
  @Output() updateSubjects: EventEmitter<any[]> = new EventEmitter();

  subjectsAltered: any = {};

  alterSubject(field: string, value: any, subjectId: number) {
    const {
      capacity,
      status,
      subjectId: originalSubjectId,
      teacherId = 0,
    } = this.group.subjects.find(({ id }) => +subjectId === +id);
    const originalSubject = {
      capacity,
      status,
      subjectId: originalSubjectId,
      teacherId,
    };
    this.subjectsAltered[subjectId] = {
      ...(this.subjectsAltered[subjectId] || originalSubject),
      [field]: value,
      subjectId,
    };
  }

  update() {
    this.updateSubjects.emit(Object.values(this.subjectsAltered));
  }

  deactivateSubjects() {
    this.group.active = false;
  }
}
