import { Component } from '@angular/core';

@Component({
  selector: 'card',
  template: `
    <div class="card">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
    `
      .card {
        border-radius: 16px;
        background: white;
        overflow: hidden;
      }
    `,
  ],
})
export class CardComponent {}
